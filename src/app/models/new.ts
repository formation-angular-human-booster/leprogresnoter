export class New {

  title: string;
  contenu: string;
  nbLike: number;
  image: string;
  constructor(title: string = null, contenu: string = null, nbLike: number = 0, image: string = null) {
    this.title = title;
    this.contenu = contenu;
    this.nbLike = nbLike;
    this.image = image;
  }
}
