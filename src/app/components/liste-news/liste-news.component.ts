import { Component, OnInit } from '@angular/core';
import {New} from '../../models/new';

@Component({
  selector: 'app-liste-news',
  templateUrl: './liste-news.component.html',
  styleUrls: ['./liste-news.component.css']
})
export class ListeNewsComponent implements OnInit {

  article1 = new New('La PS5 dévoilée', 'Très chère et très grose', 0, 'ps5.jpg');
  article2 = new New('La XBOX dévoilée', 'Le jour ou microsoft sortira un truc qui ne plante pas ça sera un clou', 0, 'xbox.jpg');

  articles = [this.article1, this.article2];
  constructor() { }

  ngOnInit(): void {
  }

}
