import {Component, Input, OnInit} from '@angular/core';
import {New} from '../../models/new';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  @Input article: New;
  nbVote = 0;
  note = 0;
  arrayStar: number[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  vote(like): void {
    this.arrayStar = [];
    this.nbVote += 1;
    if (like === '+') {
      this.article.nbLike += 5;
    }
    const note = this.article.nbLike / this.nbVote;
    this.note = Math.round(note);
    this.arrayStar = Array(this.note).fill(0).map((x, i) => i);
  }
}
